# -*- coding: utf-8 -*-

"""
imports
"""
from OpenGL import GL as gl
from OpenGL import GLU as glu
from OpenGL import GLUT as glut

import numpy as np

import sys
import os
import serial
import threading

import PIL

"""
Particle system
"""

"""
PARTICLE DEFINITION
"""
class Particle():
    
    def __init__(self, position=np.zeros(shape=3, dtype=float),
                           velocity_magnitude = 0,
                           velocity_direction=np.zeros(shape=3, dtype=float),
                           color=np.zeros(shape=3, dtype=float),
                           age=0.0,
                           life_time=0.0):
            
            self.position = position
            self.velocity_magnitude = velocity_magnitude
            self.velocity_direction = velocity_direction
            self.color = color
            self.age = age
            self.life_time = life_time
  
"""
this class contains:
    polygon for particle (n=4 vertices)
    color of particle  for every vertex
    texture of particle for evey vertex
"""          
class ParticleVertexPolygon:
    
    def __init__(self, number_of_vertices_in_polygon=4):
        
        self.number_of_vertices_in_polygon = number_of_vertices_in_polygon
        
        self.polygon = []
        for i in range(self.number_of_vertices_in_polygon):
            self.polygon.append(np.zeros(shape=3, dtype=float))
            
        self.texture = []
        for i in range(self.number_of_vertices_in_polygon):
            self.texture.append(np.zeros(shape=2, dtype=float))
            
        self.color = []
        for i in range(self.number_of_vertices_in_polygon): 
            self.color.append(np.zeros(shape=3, dtype=float))
        

class ParticleSystem():
    
    """
    constructor
    """
    def __init__(self, number_of_particles, position):
        
        self.number_of_particles = number_of_particles
        
        self.particles = []
        
        self.polygons = []
        
        self.position = position
        
        for i in range(self.number_of_particles):
            
            particle = Particle(position=self.position,
                                velocity_magnitude=np.random.rand(1)[0] * 1,
                                velocity_direction=(np.random.rand(3) * 100)/np.linalg.norm((np.random.rand(3) * 100)),
                                color=np.random.rand(3),
                                age=np.random.rand(1)[0] * 100,
                                life_time=100) 
            
            #self.configure_initial_particle_parameters(particle)
            self.particles.append(particle)
            
            # initialize polygons, texture and color, for every particle 
            self.polygons.append(ParticleVertexPolygon())
            
        # initialize polygon texture and color values
        self.map_vertex_polygon_to_particle()
            
    
    # when paricle dies it is configured again
    def configure_initial_particle_parameters(self, particle):
        
        # position - depends on wanted look, here it starts from centre of coordinate system
        particle.position = self.position
        
        # velocity magnitude - staring velocity is random
        # TODO: configure velocity magnitude
        particle.velocity_magnitude = np.random.rand(1)[0] * 1
        
        # velocity vector - is unit vector in random direction
        particle.velocity_direction = np.random.rand(3) * 100
        for i in range(3):
            if particle.velocity_direction[i] < 50:
                particle.velocity_direction[i] = - particle.velocity_direction[i]
        particle.velocity_direction /= np.linalg.norm(particle.velocity_direction)
        
        # color - random
        particle.color = np.random.rand(3)
        particle.color /= np.linalg.norm(particle.color)
        
        # age - starting age is 0
        particle.age = np.random.rand(1)[0] * 100
        
        # life_time - depends on look
        particle.life_time = 100
        
            
            
    """
    For every particle we need to define vertex if only one point will be used
    or polygon.
    self.polygons will be updated.
    """
    def map_vertex_polygon_to_particle(self):
        
        X = np.array([0.5, 0, 0])
        Y = np.array([0, 0.5, 0])
        
        for i in range(self.number_of_particles):
            
            # polygon, color and texture of particle
            particle_polygon = self.polygons[i]
            particle = self.particles[i]
            
            # polygon 
            particle_polygon.polygon[0] = particle.position + (-X -Y)
            particle_polygon.polygon[1] = particle.position + (X - Y)
            particle_polygon.polygon[2] = particle.position + (X + Y)
            particle_polygon.polygon[3] = particle.position + (-X + Y)
            
            # texture
            particle_polygon.texture[0] = np.array([0, 1])
            particle_polygon.texture[1] = np.array([1, 1])
            particle_polygon.texture[2] = np.array([1, 0])
            particle_polygon.texture[3] = np.array([0, 0])
            
            # color
            particle_polygon.color[0] = particle.color
            particle_polygon.color[1] = particle.color
            particle_polygon.color[2] = particle.color
                
                
    """
    Plot vertex/polygon/texture.
    This function must be called from display method from GL
    """
    def plot_vertex_polygon_texture_of_particle(self):
        
        gl.glBegin(gl.GL_QUADS)
        
        # for every polygon addressed to particle
        for polygon in self.polygons:
            
            color = polygon.color[0]
            
            gl.glColor3f(color[0], color[1], color[2])
            
            
            # for every vertex in polygon
            for vertex in polygon.polygon:
                

                gl.glVertex3f(vertex[0], vertex[1], vertex[2])
                       
                
        gl.glEnd()
                
        
    """
    This method must be called every delta_t time from idle function from GL
    TODO: one loop!
    """
    def new_iter(self, delta_t, interaction_force, color):
        
        # increase age
        for particle in self.particles:
            particle.age += delta_t
        
        # too old particles should be reinitialized with starting values
        for particle in self.particles:
            if particle.age > particle.life_time:
                # re-initialize particle with starting values
                self.configure_initial_particle_parameters(particle)
                pass
            
        # change particle parameters
        for particle in self.particles:
            
            # change position because of velocity
            particle.position = particle.position + particle.velocity_direction * particle.velocity_magnitude
            
            # change color of particle
            particle.color = color
            
        # update polygon for particle
        self.map_vertex_polygon_to_particle()
        
        
    """
    Debug
    """
    def debug(self):
        print("Stvoren sustav!")
        for p in self.particles:
            print(p.color)
    
        

"""
global variables
"""
colorR = 0.1
colorG = 0.1
colorB = 0.1
window = 0
t = 0  
ps = []
mouse_x = 0
mouse_y = 0

"""
GL definition and functions
"""
def initGL(width, height):
    
    gl.glClearColor(0.0, 0.0, 0.0, 0.0)
    gl.glClearDepth(1.0)
    gl.glDepthFunc(gl.GL_LESS)
    gl.glEnable(gl.GL_DEPTH_TEST)
    gl.glShadeModel(gl.GL_SMOOTH)
    
    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity()
    glu.gluPerspective(60.0, float(width)/float(height), 0.01, 100.0)
    gl.glMatrixMode(gl.GL_MODELVIEW)
    
def idle(value):
    
    global t
    global colorR
    global colorG
    global colorB
    global mouse_x
    global mouse_y
    
    colorR = colorR + 0.005
    if colorR > 1:
        colorR = 0.1
        
        colorG = colorG + 0.05
        
        if colorG > 1:
            
            colorG = 0.1
            
            colorB = colorB + 0.5
            
            if colorB > 1:
                
                colorB = 0.1
        
    color = np.array([colorR, colorG, colorB])
    
    for p in ps:
        p.new_iter(0.5, np.array([mouse_x, mouse_y]), color)
    
    glut.glutPostRedisplay()
    glut.glutTimerFunc(20, idle, 0)
  
    
    
def render_scene():
    
    gl.glPushMatrix()
    
    for p in ps:
        p.plot_vertex_polygon_texture_of_particle()
    
    
    gl.glPopMatrix()
    
    
def display():
    
    gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
    
    gl.glLoadIdentity()
    
    #Set up viewing transformation, looking down -Z axis
    gl.glLoadIdentity();
    
    gl.glTranslatef(0, 0, -100) # CENTER IS IN CENTER OF SCREEN
    
    # plot
    render_scene()
    
    glut.glutSwapBuffers()
    
def mouse(button, state, x, y):
    
    global mouse_x
    global mouse_y
    
    mouse_x = (x - 320) 

    mouse_y = (480 - y - 1 - 240) 
    
    #print(mouse_x, mouse_y)
    
    if(button == glut.GLUT_LEFT_BUTTON and state == glut.GLUT_DOWN):
        print(mouse_x, mouse_y)
        ps.append(ParticleSystem(200, np.array([mouse_x,mouse_y,30])))
            
def main():

    global window
    
    # init glut
    glut.glutInit(sys.argv)
    glut.glutInitDisplayMode(glut.GLUT_RGBA | glut.GLUT_DOUBLE | glut.GLUT_DEPTH)
    glut.glutInitWindowSize(640, 480)
    glut.glutInitWindowPosition(50, 50)
    
    # create window
    window = glut.glutCreateWindow(b'LAB2')
    
    # what to display
    glut.glutDisplayFunc(display)
    
    # mouse position
    glut.glutMouseFunc( mouse );
    
    # updates on display
    glut.glutTimerFunc(20, idle, 0) # virtual hardware dependent

    # init gl
    initGL(640, 480)
    
    glut.glutMainLoop()
    
    

        
    
"""
**************************** MAIN *********************************************
"""

if __name__ == "__main__":
    main()

    
    
    
    
    
    
    
    
    
    
    
    