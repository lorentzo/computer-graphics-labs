﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fraktal : MonoBehaviour {

	public Mesh mesh;
	public Material material;

	public int maxDepth;
	private int depth;

	public float childScale;

	public float spawnProb;

	public float maxRotationSpeed;
	private float rotationSpeed;

	public float maxTwist;

	public Gradient coloring;


	// Use this for initialization
	void Start () {

		gameObject.AddComponent<MeshFilter>().mesh = mesh;
		gameObject.AddComponent<MeshRenderer>().material = material;

		rotationSpeed = Random.Range(-maxRotationSpeed, maxRotationSpeed);
		transform.Rotate(Random.Range(-maxTwist, maxTwist), 0f, 0f);

		GetComponent<MeshRenderer>().material.color = coloring.Evaluate(0.1f);//Color.Lerp(Color.white, Color.yellow, (float) depth/maxDepth);

		if (depth < maxDepth){

			StartCoroutine(CreateChildren());

		}

	}

	private IEnumerator CreateChildren(){

		float wait1 = 1;
		float wait2 = 5;

		if(Random.value < spawnProb){
			yield return new WaitForSeconds(Random.Range(wait1,wait2));
			new GameObject("Fractal child").AddComponent<fraktal>().Initialize(this, Vector3.up);
		}

		if(Random.value < spawnProb){	
			yield return new WaitForSeconds(Random.Range(wait1, wait2));
			new GameObject("Fractal child").AddComponent<fraktal>().Initialize(this, Vector3.right);	
		}

		if(Random.value < spawnProb){
			yield return new WaitForSeconds(Random.Range(wait1, wait2));
			new GameObject("Fractal child").AddComponent<fraktal>().Initialize(this, Vector3.left);
		}

		if(Random.value < spawnProb){
			yield return new WaitForSeconds(Random.Range(wait1,wait2));
			new GameObject("Fractal child").AddComponent<fraktal>().Initialize(this, Vector3.forward);
		}

		if(Random.value < spawnProb){
			yield return new WaitForSeconds(Random.Range(wait1, wait2));
			new GameObject("Fractal child").AddComponent<fraktal>().Initialize(this, Vector3.back);
		}
		if(Random.value < spawnProb){
			yield return new WaitForSeconds(Random.Range(wait1, wait2));
			new GameObject("Fractal child").AddComponent<fraktal>().Initialize(this, Vector3.down);
		}

	}

	private void Initialize(fraktal parent, Vector3 direction){
		maxRotationSpeed = parent.maxRotationSpeed;
		maxTwist = parent.maxTwist;
		spawnProb = parent.spawnProb;
		mesh = parent.mesh;
		material = parent.material;
		maxDepth = parent.maxDepth;
		depth = parent.depth + 1; // drukcije
		childScale = parent.childScale;
		transform.parent = parent.transform;
		transform.localScale = Vector3.one * childScale; // drukcije
		transform.localPosition = direction * (0.5f + 0.5f * childScale); //drukcije
		coloring = parent.coloring;
	}
	
	// Update is called once per frame
	void Update () {
		
		transform.Rotate(0f, 30f * Time.deltaTime, 0f);
	}
}
