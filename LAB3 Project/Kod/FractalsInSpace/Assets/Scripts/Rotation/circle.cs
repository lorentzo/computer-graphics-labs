﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class circle : MonoBehaviour {

	public float ThetaScale = 0.01f;
    public float radius = 3f;
    private int Size;
    private LineRenderer LineDrawer;
    private float Theta = 0f;

	// Use this for initialization
	void Start () {

		LineDrawer = gameObject.AddComponent<LineRenderer>();
		LineDrawer.SetWidth(0.2F, 0.2F);
		
	}
	
	// Update is called once per frame
	void Update () {

		Theta = 0f;
        Size = (int)((1f / ThetaScale) + 2f);
        LineDrawer.SetVertexCount(Size); 
        for(int i = 0; i < Size; i++){          
            Theta += (2.0f * Mathf.PI * ThetaScale);         
            float x = radius * Mathf.Cos(Theta);
            float y = radius * Mathf.Sin(Theta);          
            LineDrawer.SetPosition(i, new Vector3(x, y, 0));
        }
		
	}
}
