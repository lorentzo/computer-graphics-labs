﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour {

	float timeCounter = 0;

	[Range(-3,3)]
	public float speed;
	[Range(1,10)]
	public float width;
	[Range(1,10)]
	public float height;
	[Range(1,10)]
	public float depth;
	public GameObject parentObject;


	public float ThetaScale = 0.01f;
    public float radius = 3f;
    private int Size;
    private LineRenderer LineDrawer;
    private float Theta = 0f;

    float depth_temp = 0;
    int direction = 1;
    float max_depth = 100;

    // Use this for initialization
	void Start () {

		LineDrawer = gameObject.AddComponent<LineRenderer>();
		LineDrawer.SetWidth(0.04F, 0.04F);
		LineDrawer.material.color = Color.white;
		
	}


	// Update is called once per frame
	void Update () {

		timeCounter += Time.deltaTime * speed;

		if(direction == 1){
			depth_temp += Time.deltaTime;
		}
		if(direction == 0){
			depth_temp -= Time.deltaTime;
		}
		if (depth_temp > depth){
			direction = 0;
		}
		if(depth_temp < 0){
			direction = 1;
		}

		float x = parentObject.transform.position.x + Mathf.Cos(timeCounter) * width;
		float y = parentObject.transform.position.y + Mathf.Sin(timeCounter) * height;
		float z = parentObject.transform.position.z + Mathf.Sin(timeCounter) * depth_temp;

		transform.position = new Vector3(x,y,z);

		Theta = 0f;
        Size = (int)((1f / ThetaScale) + 2f);
        LineDrawer.SetVertexCount(Size); 
        for(int i = 0; i < Size; i++){          
            Theta += (2.0f * Mathf.PI * ThetaScale);         
            float xi = parentObject.transform.position.x + width * Mathf.Cos(Theta);
            float yi = parentObject.transform.position.y + height * Mathf.Sin(Theta);   
            float zi = parentObject.transform.position.z + depth_temp * Mathf.Sin(Theta);         
            LineDrawer.SetPosition(i, new Vector3(xi, yi, zi));
        }
		
	}
}
