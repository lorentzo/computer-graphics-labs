﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraControl : MonoBehaviour {

	public float speed = 0.3f;

	private float x;
    private float y;
    private Vector3 rotateValue;

    public GameObject starter;

	// Use this for initialization
	void Start () {


		//gameObject.transform.position = starter.transform.position;
		
	}
	
	// Update is called once per frame
	void Update () {

		//gameObject.transform.position = starter.transform.position;

		 y = Input.GetAxis("Mouse X");
         x = Input.GetAxis("Mouse Y");

         rotateValue = new Vector3(x, y * -5, 0);
         transform.eulerAngles = transform.eulerAngles - rotateValue;

		if(Input.GetKey(KeyCode.RightArrow))
     {
         transform.Translate(new Vector3(speed * Time.deltaTime,0,0));
     }
     if(Input.GetKey(KeyCode.LeftArrow))
     {
         transform.Translate(new Vector3(-speed * Time.deltaTime,0,0));
     }
     if(Input.GetKey(KeyCode.DownArrow))
     {
     	 transform.position -= transform.forward * 5 * Time.deltaTime;
         //transform.Translate(new Vector3(0,-speed * Time.deltaTime,0));
     }
     if(Input.GetKey(KeyCode.UpArrow))
     {

     	 transform.position += transform.forward * 5 * Time.deltaTime;
         //transform.Translate(new Vector3(0,speed * Time.deltaTime,0));
     }
		
	}
}
