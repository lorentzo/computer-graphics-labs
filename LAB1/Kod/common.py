# -*- coding: utf-8 -*-
"""
Created on Tue Oct  2 16:58:56 2018

@author: Lovro
"""

import numpy as np
from OpenGL import GL as gl

"""
Function for reading wavefont .obj 3d objects (simplest)
"""
def read_obj(file):
    
    obj_file = open(file)
    
    vertices = []
    edges = []
    
    for line in obj_file:
        
        tokens = line.split()
        
        if len(tokens) > 0 and tokens[0] == 'v':
            vertices.append(tokens[1:4])
            
            
        elif len(tokens) > 0 and tokens[0] == 'f':
            edges.append(tokens[1:4])
            
        else:
            continue
                
    vertices = np.array(vertices, float)
    edges = np.array(edges, int)
    
    return vertices, edges


"""
Function for drawing object
"""
def draw_obj(edges, vertices, color=[0.0, 1.0, 1.0]):
    
    gl.glBegin(gl.GL_LINE_LOOP)
    
    gl.glColor3f(color[0], color[1], color[2])
    
    for edge in edges:
        
        for v_i in edge:
            
            gl.glVertex3f(vertices[v_i-1][0],
                          vertices[v_i-1][1],
                          vertices[v_i-1][2])
    
          
    gl.glEnd()
         
            

"""
Function for reading (3d) coordinates from file
"""
def read_coords(file):
    
    coords_file = open(file)
    
    coordinates = []
    
    for line in coords_file:
        
        tokens = line.split()
        
        coordinates.append(tokens)
        
    coordinates = np.array(coordinates, float)
    
    return coordinates