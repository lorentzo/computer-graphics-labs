
"""
Created on Tue Oct  2 15:52:06 2018

@author: Lovro
"""

"""
*********************************** Imports ***********************************
"""

import numpy as np

from OpenGL import GL as gl
from OpenGL import GLU as glu
from OpenGL import GLUT as glut

import glm

import common

import sys
import os
import serial
import threading

"""
**************************** global variables *********************************
"""
# window id
window = 0

# parameter of curve segment defining the point
t = 0
s = 1

# control points defining the curve
control_points_file = 'control_points.txt'
control_points = common.read_coords(control_points_file)

n = len(control_points)

"""
**************************** openGL part **************************************
"""

def initGL(width, height):
    
    gl.glClearColor(0.0, 0.0, 0.0, 0.0)
    gl.glClearDepth(1.0)
    gl.glDepthFunc(gl.GL_LESS)
    gl.glEnable(gl.GL_DEPTH_TEST)
    gl.glShadeModel(gl.GL_SMOOTH)
    
    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity()
    glu.gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)
    gl.glMatrixMode(gl.GL_MODELVIEW)
    
def idle(value):
    
    global t
    global s
    global n
    
    t = t + 1 / 50
    
    if t > 1:
        
        t = 0
        s = s + 1
        
        if s >= n-3 + 1:
            s = 1
        
    glut.glutPostRedisplay()
    glut.glutTimerFunc(20, idle, 0)
    
    
def render_scene():
    
    # plotting the curve
    gl.glPushMatrix()
    
    gl.glTranslatef(0, 0, -100)
    gl.glRotate(30, 10.0, 4.0, 3.0)
    
    BSpline_cube_curve(control_points, 50)
    
    gl.glPopMatrix()
    
    
    # plotting the tangent on curve
    gl.glPushMatrix()
    
    gl.glTranslatef(0, 0, -100)
    gl.glRotate(30, 10.0, 4.0, 3.0)
    
    curve_tangent(control_points, 7)
    
    gl.glPopMatrix()
    
    
    # animating object
    gl.glPushMatrix()
    
    gl.glTranslatef(0, 0, -100)
    gl.glRotate(30, 10.0, 4.0, 3.0)
    
    # animate using glrotatef()
    animate(control_points, s)
    
    # animate using DCM
    #animate_DCM(control_points, s)
    
    gl.glPopMatrix()
    
def display():
    
    gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
    
    gl.glLoadIdentity()
    
    render_scene()
    
    glut.glutSwapBuffers()
    
  
def main():
    
    global window
    
    glut.glutInit(sys.argv)
    glut.glutInitDisplayMode(glut.GLUT_RGBA | glut.GLUT_DOUBLE | glut.GLUT_DEPTH)
    glut.glutInitWindowSize(640, 480)
    glut.glutInitWindowPosition(50, 50)
    
    window = glut.glutCreateWindow(b'LAB1')
    
    glut.glutDisplayFunc(display)
    
    glut.glutTimerFunc(20, idle, 0) # virtual hardware dependent
    
    initGL(640, 480)
    
    glut.glutMainLoop()


"""
********************************* B-spline ************************************
Properties:
        + approximate
        + uniform
        + cube (k=3)
        + arbitrary no of control points (n)
        + periodic segment representation (4 control points)
        + parametric segment t in [0,1]
        + ...
"""

"""
# for segment i of curve calculate curve point parametrized with t
"""
def cube_curve_segment(t, control_points, i_segment):
    
    # parameter t
    T = np.array([t * t * t, t * t, t, 1])
    
    # periodic segment
    B = 1/6 * np.array([[-1, 3, -3, 1],
                        [3, -6, 3, 0], 
                        [-3, 0, 3, 0], 
                        [1, 4, 1, 0]])

    # control points for current segment
    R = np.array([control_points[i_segment-1],
                  control_points[i_segment],
                  control_points[i_segment+1],
                  control_points[i_segment+2]])

    # point on current segment for given t
    TB = np.dot(T,B)
    TBR = np.dot(TB, R)
    
    return TBR

"""
# for curve segment i calculate tangent in point parametrized with t
"""
def cube_curve_segment_tangent(t, control_points, i_segment):
    
    # parameter
    T = [t*t, t, 1]
    
    # periodic segment
    B = 1/2 * np.array([[-1, 3, -3, 1], 
                        [2, -4, 2, 0],
                        [-1, 0, 1, 0]])
    
    #control points for current segment
    R = np.array([control_points[i_segment-1],
                  control_points[i_segment],
                  control_points[i_segment+1],
                  control_points[i_segment+2]])
    
    # tangent vector for current point
    TB = np.dot(T, B)
    TBR = np.dot(TB, R)
    
    return TBR

"""
# for curve segment i calculate 2nd derivative vector in point parametrized with t
"""
def curve_diff2(t, control_points, i_segment):
    
    # parameter
    T = [2*t, 1]
    
    # periodic segment
    B = 1/2 * np.array([[-1, 3, -3, 1],
                        [2, -4, 2, 0]])
    
    #control points for current segment
    R = np.array([control_points[i_segment-1],
                  control_points[i_segment],
                  control_points[i_segment+1],
                  control_points[i_segment+2]])
    
    # second derivative vector for current point
    TB = np.dot(T, B)
    TBR = np.dot(TB, R)
    
    return TBR
"""
# given tangent and 2nd derivative vectors calculate rotation matrix and return inverse
"""
def rotation_matrix(tangent_vector, diff2_vector):
    
    w = tangent_vector
    w = w / np.linalg.norm(w)
    
    u = np.cross(tangent_vector, diff2_vector)
    u = u / np.linalg.norm(u)
    
    v = np.cross(w, u)
    v = v / np.linalg.norm(v)
    
    R = np.column_stack([w, u, v])
    
    R_inv = np.linalg.inv(R)
    
    return R_inv
    
"""
# for given current and next orientation, function returnes rotation parameters (axis and angle)
"""
def rotation_parameters(curr_orient, next_orient):
    
    rotation_axis = np.cross(curr_orient, next_orient)
    
    dot_curr_next = np.dot(curr_orient, next_orient)
    curr_mag = np.linalg.norm(curr_orient)
    next_mag = np.linalg.norm(next_orient)
    
    rotation_angle = np.rad2deg(np.arccos(dot_curr_next / (curr_mag * next_mag)))
    
    return rotation_axis, rotation_angle
    
""" 
# function for plotting whole B-spline curve
"""
def BSpline_cube_curve(control_points, resolution):
    
    n = len(control_points)
    
    gl.glBegin(gl.GL_LINE_STRIP)
    gl.glColor3f(0.0, 1.0, 0)
    
    # for all segments
    for i in range(1, n-3 + 1):
        
        # for all points (parametrized with t) for current segment
        for t in np.linspace(0, 1, resolution):
            
            # for current segment, calculate point parametrized with current t
            segment_point = cube_curve_segment(t, control_points, i)
            print(segment_point)
            
            #plot object on current segment_point
            gl.glVertex3f(segment_point[0], segment_point[1], segment_point[2])   
            
    gl.glEnd()
            
"""
# function for plotting tangent on curve
"""
def curve_tangent(control_points, resolution):
    
    n = len(control_points)
    
    gl.glBegin(gl.GL_LINES)
    gl.glColor3f(1.0, 0.0, 0.0)

    # for all segments
    for i in range(1, n-3 + 1):
        
        # for all points (parametrized with t) for current segment
        for t in np.linspace(0, 1, resolution):
            
            # for current segment, calculate current point parametrized with current t
            segment_point = cube_curve_segment(t, control_points, i)
            
            # tangent for current point
            segment_tangent = cube_curve_segment_tangent(t, control_points, i)
            
            # draw a tangent line
            k = 0.5
            
            gl.glVertex3f(segment_point[0], segment_point[1], segment_point[2])
            
            gl.glVertex3f(segment_point[0] + segment_tangent[0] * k,
                          segment_point[1] + segment_tangent[1] * k, 
                          segment_point[2] + segment_tangent[2] * k)
            
            
    gl.glEnd()
     
"""        
# function for animating the object using current and next orientation vector
which results in axis and angle of rotation used by gltranslatef()
"""
def animate(control_points, i_segment):
    
    #starting orientation of object
    curr_orient = np.array([0, 0, 1])
    
    # read object to animate
    obj_file = "kocka.obj"
    obj_vertices, obj_edges = common.read_obj(obj_file)
            
    # for current segment, calculate current point parametrized with current t
    segment_point = cube_curve_segment(t, control_points, i_segment)
    
    # tangent for current point
    segment_tangent = cube_curve_segment_tangent(t, control_points, i_segment)
    
    #rotation params
    rot_axis, rot_angle = rotation_parameters(curr_orient, segment_tangent)
    curr_orient = segment_tangent
    
    # plotting
    
    gl.glPushMatrix()
    
    gl.glTranslatef(segment_point[0],
                    segment_point[1],
                    segment_point[2])
    
    gl.glScalef(5, 5, 5)
    
    gl.glRotate(rot_angle, rot_axis[0], rot_axis[1], rot_axis[2])
    
    common.draw_obj(obj_edges, obj_vertices)
    
    gl.glPopMatrix()

       
"""
Animate object. Rotation is performed using rotation matrix which is 
calculated by current position and wanted coordinate system (which is rotated)
"""       
def animate_DCM(control_points, i_segment):
    
    # read object to animate
    obj_file = "kocka.obj"
    obj_vertices, obj_edges = common.read_obj(obj_file)
            
    # for current segment, calculate current point parametrized with current t
    curr_point = cube_curve_segment(t, control_points, i_segment)
    
    # tangent for current point
    curr_point_tangent = cube_curve_segment_tangent(t, control_points, i_segment)
    
    # 2nd derivative vector
    curr_point_diff2 = curve_diff2(t, control_points, i_segment)
    
    # rotation matrix
    R = rotation_matrix(curr_point_tangent, curr_point_diff2)
  
    # plot
    gl.glPushMatrix()
    
    obj_vertices_rot = []
    for vertex in obj_vertices:
        v = np.array(vertex)
        v_rot = np.ndarray.tolist( np.dot(v, R) )
        if np.isnan(v_rot[0]): #2nd derivative is 0
            v_rot = np.random.rand(3)
            v_rot = v_rot / np.linalg.norm(v_rot)
        obj_vertices_rot.append(v_rot)
        print(v_rot)
        
    gl.glTranslatef(curr_point[0],
                    curr_point[1],
                    curr_point[2])
        
    gl.glScalef(5, 5, 5)  
    
    # PAZI! Direkno racunaj s vrhovima
    common.draw_obj(obj_edges, obj_vertices_rot)
    
    gl.glPopMatrix()
    

"""
**************************** MAIN *********************************************
"""

if __name__ == "__main__":
    main()



